package main

import "fmt"

type Address struct {
	City, Province, Country string
}

// pointer di function
func ChangeCountryToIndo(address *Address) {
	address.Country = "Indonesia"
}

func main() {
	address1 := Address{"Batang", "jawa Tengah", "indo"}
	address3 := &address1
	// pass by value
	// address2 := address1
	// pass by refrense tombol &
	address2 := &address1
	address2.City = "Bandung"
	// operator untuk mengubah referemsi yang ada di Address menjadi adrres 2
	*address2 = Address{"Van", "jawa Tengah", "indo"}
	fmt.Println(address1)
	fmt.Println(address2)
	fmt.Println(address3)
	address4 := new(Address)
	address4.City = "Jakarta"
	fmt.Println(address4)
	alamat := Address{"Batang", "jawa tengah", ""}
	ChangeCountryToIndo(&alamat)
	fmt.Println(alamat)
}
