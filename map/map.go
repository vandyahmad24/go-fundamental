package main

import "fmt"

func main() {
	person := map[string]string{
		"name":    "vandy",
		"address": "Batang",
	}
	// tambah data
	person["title"] = "programmer"
	fmt.Println(person)
	fmt.Println(person["name"])
	fmt.Println(len(person))
	// membuat map tanpa data apapun
	book := make(map[string]string)
	book["title"] = "belajar golang"
	book["author"] = "vandy"
	book["salah"] = "salah"
	fmt.Println(book)
	delete(book, "salah")
	fmt.Println(book)

}
