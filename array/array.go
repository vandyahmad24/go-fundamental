package main

import "fmt"

func main() {
	// array
	// pada golang data pada array ditentukan diawal
	var name [3]string
	name[0] = "vandy"
	name[1] = "ahmad"
	name[2] = "misri"
	// name[4]
	fmt.Println(name[0])
	// deklrasi array dengan isi data
	var values = [...]string{
		"ganteng", "ahmad", "gagah", "ganteng",
	}
	values[1] = "bambang"
	fmt.Println(values)
	fmt.Println(len(values))
	// merubah data

}
