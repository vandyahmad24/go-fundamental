package main

import "fmt"

type Customer struct {
	Name, Address string
	Age           int
}

// struct method
func (customer Customer) sayHello() {
	fmt.Println("Hello, My Name is", customer.Name)
}

func main() {
	// struct adalah sebuah template data untuk menggabungkan berbagai tipe data
	// struct adalah template data
	// struct tidak bisa langsung digunakan
	// struct mirip seperti object

	var vandy Customer
	vandy.Name = "Vandy"
	vandy.Address = "Indonesia"
	vandy.Age = 30
	fmt.Println(vandy.Name)
	// struct literals
	joko := Customer{
		Name:    "Jokoiw",
		Address: "jalan",
		Age:     12,
	}
	fmt.Println(joko)
	// struct method bisa digunakan sebagai paramter difunction
	// namun jika ingin menambahkan method ke dalam struct sehingga seakan akan sebuah struct memiliki function
	joko.sayHello()

}
