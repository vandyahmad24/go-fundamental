package main

import "fmt"

// merubah tipe interface kosong jd tipe yg kita inginkan

func random() interface{} {
	return true
}
func main() {
	result := random()
	switch value := result.(type) {
	case string:
		fmt.Println("value is string", value)
	case int:
		fmt.Println("value is int", value)
	default:
		fmt.Println("value tidak ditemukan")
	}
}
