package main

import "fmt"

func newMap(name string) map[string]string {
	if name == "" {
		return nil
	} else {
		mapVar := map[string]string{
			"name": name,
		}
		return mapVar
	}
}

func main() {
	data := newMap("va")
	if data == nil {
		fmt.Println("datanya kosong")
	} else {
		fmt.Println(data["name"])
	}

}
