package main

import "fmt"

func main() {
	// defer function adalah function yg bisa kita jadwalkan untuk dieksekusi setelah sebuah function selesai dieksekusi
	// defer function akan selalu dieksekusi walaupun terjadi error difunction yg dieksekusi
	runApplication(10)
	// panic function adalah fungsi yg bisa kita gunakan untuk menghentikan program
	runApp(true)
	// recover adalah untuk menangkap data panic
	// proses panic tidak akan berhenti
	fmt.Println("Vandy")
}

func logging() {
	fmt.Println("Selesai memanggil logging")
}

func endApp() {

	message := recover()
	if message != nil {
		fmt.Println("Error dengan message", message)
	}
	fmt.Println("App End")

}
func runApp(error bool) {
	defer endApp()
	if error {
		panic("APLIKASI ERROR")
	}

	fmt.Println("Aplikasi Berjalan")
}

// fungsinya diawal menggunakan defer
func runApplication(value int) {
	defer logging()
	fmt.Println("Run Application")

	result := 10 / value
	fmt.Println("Result", result)

}
