package main

import "fmt"

func main() {
	// var -> nama variabel -> type data
	var bambang int
	bambang = 2
	fmt.Println(bambang)
	bambang = 30
	fmt.Println(bambang)
	// tanpa menyebutkan tipe data
	var name = "ahmad ganteng"
	fmt.Println(name)
}
