package main

import "fmt"

func main() {
	// slice pada golang seperti array tapi ukuran bisa berubah -> potongan data dari array
	// memiliki pointer, length dan capacity
	// pointer -> penunjuk data pertama diarray pada slice
	// lenght panjang dari slice
	// capacity akapsitas slice lenght tidak boleh melebihi capacity
	// membuat slice -> array[low:high] -> dimulai index ke low sampai index sebelum high
	// 					array[low:] -> dimulai dari index low sampai index khir diarray
	// 					array[:high] -> dimulai dari 0 sampai index high
	var bulan = [...]string{
		"januari", "februari", "maret", "april", "mei", "juni", "juli", "agustus", "september", "oktober", "november", "desember",
	}
	var slice1 = bulan[4:7]
	fmt.Println(slice1)
	// liat kapasitas
	fmt.Println(len(slice1))
	fmt.Println(cap(slice1))
	bulan[5] = "cah embuh"
	fmt.Println(slice1)

	// buat slice baru
	var slice2 = bulan[10:]
	fmt.Println(slice2)
	var slice3 = append(slice2, "bambang")
	fmt.Println(slice3)
	slice3[1] = "bukan desember"
	fmt.Println(slice3)
	fmt.Println(slice2)
	fmt.Println(bulan)

	// make new slice
	var newSlice = make([]string, 2, 5)
	newSlice[0] = "vandy slice"
	newSlice[0] = "vandy kkwk"
	fmt.Println(newSlice)
	// copy slice
	var copySlice = make([]string, len(newSlice), cap(newSlice))

	copy(copySlice, newSlice)
	fmt.Println(copySlice)
}
