package main

import "fmt"

// buat kontrak dalam bentuk Interface
type HasName interface {
	// jika dia hasname maka harus pnya get name dengan return string
	GetName() string
}

func SayHello(hasName HasName) {
	fmt.Println("Hello", hasName.GetName())
}

type Person struct {
	Name string
}

func (person Person) GetName() string {
	return person.Name
}

func main() {
	// interfaces adalah tipe data abstract tidak memiliki implementasi langsung
	// sebuah interfaces berisi definisi method
	// interfaces digunakan sebagai kontrak
	// implementasi setiap tipe data yg sesuai dengan kontrak interface secara otomatis dianggap sebagai interface tersebut sehingga kita tidak perlu implementasikan manual
	vandy := Person{
		Name: "ahmad",
	}
	// fmt.Println(vandy)
	SayHello(vandy)

}
