package main

import "fmt"

func Ups(i int) interface{} {
	if i == 1 {
		return 1
	} else if i == 2 {
		return true
	} else {
		return "Ups"
	}
	// return 1
	// return true

}

func main() {
	kosong := Ups(2)
	fmt.Println(kosong)
}
