package main

import "fmt"

func main() {
	fmt.Println(loopFaktorial(5))
	fmt.Println(factorialRecursive(5))
}

// dengan loop biasa
func loopFaktorial(value int) int {
	result := 1
	for i := value; i > 0; i-- {
		result *= i
	}

	return result
}

// dengan recursive function
func factorialRecursive(value int) int {
	if value == 1 {
		fmt.Println("Berhenti di satu")
		return 1
	} else {
		fmt.Println("perulangna disini", value)
		return value * factorialRecursive(value-1)
	}
}
