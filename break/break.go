package main

import "fmt"

func main() {
	// break digunakan untuk menghentikan seluruh baris code
	// continue dilewati atau skip pada saatt kondisi saja
	// break
	for i := 0; i <= 10; i++ {
		fmt.Println("Perulangna ke", i)
		if i == 5 {
			break
		}

	}
	// continue
	for i := 0; i < 10; i++ {

		if i%2 == 0 {
			continue
		}
		fmt.Println("Perulangan ganjil ke", i)
	}
}
