package main

import "fmt"

func main() {
	// hanya ada for loop
	counter := 1

	for counter <= 10 {
		fmt.Println("perulangan ke", counter)
		counter++
	}
	// for statmen
	for i := 1; i <= 10; i++ {
		fmt.Println("perulangan kedua", i)
	}
	// for untuk arry
	slice := []string{"vandy", "ahmad", "misry"}
	for i := 0; i < len(slice); i++ {
		fmt.Println("slice", slice[i])
	}
	// for range
	names := []string{"lutfi", "alif", "vanyd", "ganteng"}
	for _, name := range names {
		fmt.Println("names:", name)
	}
	person := make(map[string]string)
	person["name"] = "vandy"
	person["name2"] = "ahmad"
	for index, value := range person {
		fmt.Println("index", index, "value", value)
	}

}
