package main

import "fmt"

func main() {
	const link = "www.google.com"
	fmt.Println(link)
	//  multi constant
	const (
		namaApp = "buka app"
		versi   = "1.23"
	)

}
