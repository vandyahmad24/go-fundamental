package main

import "fmt"

func main() {
	sayHello()
	sapaTamu("vandy", "febri", 17)

	hasil := hitungMatematika(10, 20, "-")
	fmt.Println(hasil)
	// ambil multiple value
	firstName, lastName, _ := getFullBio()
	fmt.Println(firstName)
	fmt.Println(lastName)
	namaAwal, namaTengah, namaAkhir := getCompleBio()
	fmt.Println(namaAwal)
	fmt.Println(namaTengah)
	fmt.Println(namaAkhir)
	// Variadic Function,
	// paramter yang berada diposisi terakhir memiliki kemampuan untuk dijadikan varags (variabel argument)
	// func yg memiliki varargs disebut variadic function
	// variabel argument dapat menampung banyak variabel
	sum := sumAll("Penjumlahan", 2, 341, 23, 12, 123, 13, 123, 13, 2123, 1, 31)
	fmt.Println(sum)
	// dengan slice
	slice := []int{2, 1, 3, 12, 3, 12}
	total := sumAll("Penambahan", slice...)
	fmt.Println(total)
	// function variabel
	// function yg masuk ke dalam variabel
	sayGoodBye := getGoodBye
	fmt.Println(sayGoodBye("ahmad"))
	// function parameter
	filter := filterWord
	getHelloWithFilter("man", filter)
	getHelloWithFilter("anjing", filter)
	getHelloWithFilterTypeAlias("anjing", filter)
	// dengan type alias
	// function anonymous
	blacklist := func(name string) bool {
		return name == "admin"
	}
	registerUser("admin", blacklist)
	// anonimous function dengan paramter
	registerUser("admin", func(name string) bool {
		return name == "root"
	})

}

type Filter func(string) string
type BlackList func(string) bool

func sayHello() {
	fmt.Println("halo")
}
func getGoodBye(name string) string {
	return "selamat jalan " + name
}

// function anonymouse
func registerUser(name string, blacklist BlackList) {
	if blacklist(name) {
		fmt.Println("Your are blocked", name)
	} else {
		fmt.Println("Welcome", name)
	}
}

// function parameter
// filter func(string) <- merupakan pemangilan ke fungsi lain
func getHelloWithFilter(name string, filter func(string) string) {
	fmt.Println("Halooo", filter(name))
}

// dengan type alias
func getHelloWithFilterTypeAlias(name string, filter Filter) {
	fmt.Println("Halooo type alias", filter(name))
}

func filterWord(name string) string {
	if name == "anjing" {
		return "****"
	} else {
		return name
	}
}

// func parameter
func sapaTamu(tamuPertama string, tamuKedua string, biaya int) {
	fmt.Println("Selamat datang", tamuPertama, "dan", tamuKedua, "Biaya anda", biaya)
}

// func return value

func hitungMatematika(angkaPertama int, angkaKedua int, operasi string) int {
	hasil := 0
	switch operasi {
	case "+":
		hasil = angkaPertama + angkaKedua
	case "-":
		hasil = angkaPertama - angkaKedua
	default:
		hasil = 0
	}
	return hasil
}

//return multiple value
func getFullBio() (string, string, int) {
	return "Vandy", "Ahmad", 23
}

// named return values
func getCompleBio() (firstName, middleName, lastName string) {
	firstName = "vandy"
	middleName = "ahmad"
	lastName = "misri"
	return
}

// variadic function cirinya titik 3
func sumAll(tipe string, numbers ...int) int {
	total := 0
	for _, number := range numbers {
		total += number
	}
	fmt.Println(tipe)
	return total
}
